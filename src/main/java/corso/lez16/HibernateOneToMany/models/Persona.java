package corso.lez16.HibernateOneToMany.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity					
@Table(name="persona")	
public class Persona {

	@Id					
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PersonaID")
	private int id;
	
	@Column				//Dato che l'attributo ha lo stesso nome della colonna, evito di utilizzare il name!
	private String nome;
	@Column
	private String cognome;
	@Column
	private String telefono;
	@Column
	private String indirizzo;
	
	@OneToMany(mappedBy = "proprietario")
	private List<Carta> elencoCarte;
	
	public Persona() {
		this.elencoCarte = new ArrayList<Carta>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getIndirizzo() {
		return indirizzo;
	}
	
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	public List<Carta> getElencoCarte() {
		return elencoCarte;
	}

	public void setElencoCarte(List<Carta> elencoCarte) {
		this.elencoCarte = elencoCarte;
	}
	
	public String stampaPersona() {
		return "Persona [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", telefono=" + telefono
				+ ", indirizzo=" + indirizzo + ", elencoCarte=" + elencoCarte + "]";
	}
	
	public String dettaglioPersonaSenzaRiferimento() {
		return "Persona [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", telefono=" + telefono
				+ ", indirizzo=" + indirizzo + "]";
	}
	
	
	
}
