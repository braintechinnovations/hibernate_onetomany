package corso.lez16.HibernateOneToMany.models.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import corso.lez16.HibernateOneToMany.models.Carta;
import corso.lez16.HibernateOneToMany.models.Persona;

public class GestoreSessioni {
	private SessionFactory factory;
	private static GestoreSessioni ogg_gestore;
	
	public static GestoreSessioni getGestore() {
		if(ogg_gestore == null) {
			ogg_gestore = new GestoreSessioni();
		}
		
		return ogg_gestore;
	}

	public SessionFactory getFactory() {
		if(factory == null) {
			factory = new Configuration()
					.configure("/resources/hibernate_negozio.cfg.xml")
					.addAnnotatedClass(Persona.class)
					.addAnnotatedClass(Carta.class)
					.buildSessionFactory();
		}
		
		return factory;
	}
}
