package corso.lez16.HibernateOneToMany.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="carta")
public class Carta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CartaID")
	private int id;
	
	@Column
	private String codice;
	@Column
	private String negozio;
	
	@ManyToOne
	@JoinColumn(name="PersonaRif")			//Collegamento tramite FK
	private Persona proprietario;
	
	public Carta() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNegozio() {
		return negozio;
	}

	public void setNegozio(String negozio) {
		this.negozio = negozio;
	}

	public Persona getProprietario() {
		return proprietario;
	}

	public void setProprietario(Persona proprietario) {
		this.proprietario = proprietario;
	}
	
	public String stampaCarta() {
		return "Carta [id=" + id + 
				", codice=" + codice + 
				", negozio=" + negozio  + 
				", proprietario=" + proprietario.dettaglioPersonaSenzaRiferimento() + 
				"]";
	}

	@Override
	public String toString() {
		return "Carta [id=" + id + ", codice=" + codice + ", negozio=" + negozio + "]";
	}

	
}
