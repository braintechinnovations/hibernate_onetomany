package corso.lez16.HibernateOneToMany.controller;

import corso.lez16.HibernateOneToMany.crud.CartaDAO;
import corso.lez16.HibernateOneToMany.crud.PersonaDAO;
import corso.lez16.HibernateOneToMany.models.Carta;

public class CartaController {
	
	private CartaDAO carDao;
	private PersonaDAO perDao;
	
	public CartaController(){
		carDao = new CartaDAO();
		perDao = new PersonaDAO();
	}

	public boolean inserisciCarta(String codice, String negozio) {
	
		Carta temp = new Carta();
		temp.setCodice(codice);
		temp.setNegozio(negozio);
		
		this.carDao.insertion(temp);
		
		if(temp.getId() > 0){
			return true;
		}
		
		return false;	
	}
	
	public boolean inserisciCarta(String codice, String negozio, int idPersona) {
		
		Carta temp = new Carta();
		temp.setCodice(codice);
		temp.setNegozio(negozio);
		temp.setProprietario(perDao.findById(idPersona));
		
		this.carDao.insertion(temp);
		
		if(temp.getId() > 0){
			return true;
		}
		
		return false;
	}
	
}
