package corso.lez16.HibernateOneToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez16.HibernateOneToMany.controller.CartaController;
import corso.lez16.HibernateOneToMany.models.Carta;
import corso.lez16.HibernateOneToMany.models.Persona;
import corso.lez16.HibernateOneToMany.models.db.GestoreSessioni;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

//    	//TODO: Ristrutturatelo con il DAO e Controller ;)
//    	
//    	SessionFactory factory = GestoreSessioni.getGestore().getFactory();
//		Session sessione = factory.getCurrentSession();
//		
//		try {
////			sessione.beginTransaction();	
//			
////			Persona art = new Persona();
////			art.setNome("Artabano");
////			art.setCognome("Pastrocchio");
////			art.setIndirizzo("Via tevere, 5");
////			art.setTelefono("123456");
////			
////			Carta cartUno = new Carta();
////			cartUno.setCodice("COD123456");
////			cartUno.setNegozio("CONAD");
////			cartUno.setProprietario(art);
////
////			Carta cartDue = new Carta();
////			cartDue.setCodice("CAR965468");
////			cartDue.setNegozio("COOP");
////			cartDue.setProprietario(art);
////			
////			art.getElencoCarte().add(cartUno);
////			art.getElencoCarte().add(cartDue);
////			
////			sessione.save(art);
////			sessione.save(cartUno);
////			sessione.save(cartDue);
//			
//			//REPERIMENTO PERSONA
////			int id = 41;				//Exception
////			Persona risultato = sessione.get(Persona.class, id);
////			
////			System.out.println(risultato.stampaPersona());
//
////			int id = 8;
////			Carta risultato = sessione.get(Carta.class, id);
////			
////			System.out.println(risultato.stampaCarta());
//			
////			sessione.getTransaction().commit();	
////			
////			System.out.println("Sessione terminata");
//			
//			//--------------------------------------------------------------------
//			
//		} catch (Exception errore) {
//			System.out.println(errore.getMessage());
//		} finally {
//			sessione.close();					
//			System.out.println("Connessione Chiusa!");
//		}
    	
    	//REFACTOR UTILIZZANDO I CONTROLLER 
    	
    	CartaController carCon = new CartaController();
    	
    	if(carCon.inserisciCarta("FG456789", "Tigre", 41)) {
    		System.out.println("Operazione effettuata con successo");
    	}
    	else {
    		System.out.println("Errore di esecuzione");
    	}

    }
}
