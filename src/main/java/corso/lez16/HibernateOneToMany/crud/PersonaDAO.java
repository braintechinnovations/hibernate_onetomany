package corso.lez16.HibernateOneToMany.crud;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez16.HibernateOneToMany.models.Persona;
import corso.lez16.HibernateOneToMany.models.db.GestoreSessioni;

public class PersonaDAO implements Dao<Persona> {

	@Override
	public void insertion(Persona t) {

		SessionFactory factory = GestoreSessioni.getGestore().getFactory();
		Session sessione = factory.getCurrentSession();
		
		try {
			sessione.beginTransaction();			//1. Avvio una nuova transazione
			
			sessione.save(t);					//2. Effettuo tutte le operazioni che voglio
			
			sessione.getTransaction().commit();		//3. Se tutte le operazioni vanno a buon fine allora effettuo la Commit! Altrimenti, Rollback!
			
			System.out.println(t.toString());
			
		} catch (Exception errore) {
			System.out.println(errore.getMessage());
		} finally {
			sessione.close();						//Alla fine dell'utilizzo della sessione, chiudo!
			System.out.println("Connessione Chiusa!");
		}
		
	}

	@Override
	public Persona findById(int id) {
		SessionFactory factory = GestoreSessioni.getGestore().getFactory();
		Session sessione = factory.getCurrentSession();
		
		Persona risultato = null;	
		
		try {
			sessione.beginTransaction();
			
			risultato = sessione.get(Persona.class, id);
			
			sessione.getTransaction().commit();
		
		} catch (Exception e) {
			System.out.println("Errore: " + e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Connessione chiusa!");
		}
		
		return risultato;
	}

	
	
}
