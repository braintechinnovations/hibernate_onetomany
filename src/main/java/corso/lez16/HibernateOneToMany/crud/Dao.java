package corso.lez16.HibernateOneToMany.crud;

public interface Dao<T> {
	
	void insertion(T t);
	
	T findById(int id);
	
	//TODO: ... altri metodi
}
